/**
 * Google Maps loader,
 * Window resize handler
 * And slideshow creater
 *
 * @author Marlin Cremers - m.cremers@digital-ground.nl
 * @version 1.0
 */
var warning = {
    dontShow: false,
    showWarning: function () {
        if (!this.dontShow) {
            $('#warning').fadeIn(750);
        }
    },
    closeWarning: function () {
        $('#warning').fadeOut(750);
        this.dontShow = true;
    }
};
var map = {
    geocoder: null,
    loadMapScript: function () {
        $.getScript("http://maps.googleapis.com/maps/api/js?sensor=false&callback=map.initializeMap&language=nl");
    },
    initializeMap: function () {
        this.geocoder = new google.maps.Geocoder();
        this.setLatLng('Frankrijkweg 19 5975 PC Sevenum', this.loadMap);
    },
    loadMap: function (results) {
        latLng = results[0].geometry.location;
        var myOptions = {
          zoom: 14,
          center: latLng,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var map = new google.maps.Map(document.getElementById('roadmap'),
            myOptions);
        var marker = new google.maps.Marker({
            position: latLng, 
            map: map, 
            title:'Rue de France',
            icon:'images/maps_icon.png'
        });  
        var infoWindow = new google.maps.InfoWindow({
            content: 'Rue de France'
        });
        infoWindow.open(map,marker);

    },
    setLatLng: function (address, callback) {
        this.geocoder.geocode({'address': address}, callback);
    }
}
var slideshow = {
    pictures: ['images/slideshow/3972.jpg', 'images/slideshow/3974.jpg',
               'images/slideshow/3979.jpg', 'images/slideshow/3982.jpg',
               'images/slideshow/3988.jpg', 'images/slideshow/3992.jpg',
               'images/slideshow/3996.jpg', 'images/slideshow/4008.jpg'],
    picturesBig: ['images/pictures/3972.jpg', 'images/pictures/3974.jpg',
               'images/pictures/3979.jpg', 'images/pictures/3982.jpg',
               'images/pictures/3988.jpg', 'images/pictures/3992.jpg',
               'images/pictures/3996.jpg', 'images/pictures/4008.jpg'],
    currentPicture: 0,
    enabled: 0,
    initialize: function () {
        if ($('#slideshow').length) {
            $('#slideshow').height(200);
            $('#slideshow').width(150);
            $('#slideshow').css('background-image', 'url("' + slideshow.getPictureURI(slideshow.currentPicture) + '")');
        }
        if ($('#slideshow-big').length) {
            $('#slideshow-big').height(400);
            $('#slideshow-big').width(533);
            $('#slideshow-big').css('background-image', 'url("' + slideshow.getPictureURI(slideshow.currentPicture) + '")');
        }
        $(document).everyTime(3500, slideshow.loadNextPicture);
        slideshow.enabled = 1;
    },
    loadNextPicture: function () {
        if (!slideshow.enabled) {
            return;
        }
        ++slideshow.currentPicture;
        if (!slideshow.getPictureURI(slideshow.currentPicture)) {
            slideshow.currentPicture = 0;
        }
        if ($('#slideshow').length) {
            $('#slideshow').css('background-image', 'url("' + slideshow.getPictureURI(slideshow.currentPicture) + '")');
        }
        if ($('#slideshow-big').length) {
            $('#slideshow-big').css('background-image', 'url("' + slideshow.getPictureURI(slideshow.currentPicture, true) + '")');
        }
    },
    getPictureURI: function (pictureId, big) {
        if (big) {
            return slideshow.picturesBig[pictureId];
        } else {
            return slideshow.pictures[pictureId];
        }
    },
    showImage: function (pictureURI) {
        if ($('#slideshow-big').length) {
            slideshow.enabled = 0;
            $('#slideshow-big').css('background-image', 'url("' + pictureURI + '")');
        }
    }
}
$(window).resize(function () {
    if (($(window).height() < 500) || ($(window).width() < 900)) {
        warning.showWarning();
    }
    else {
        warning.closeWarning();
    }
});
$(window).load(function() {
    if ($('#roadmap').length) {
        map.loadMapScript();
    }
    if (($('#slideshow').length) || ($('#slideshow-big').length)) {
        slideshow.initialize();
    }
    if (($(window).height() < 500) || ($(window).width() < 900)) {
        warning.showWarning();
    }
});