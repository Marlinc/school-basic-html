<!DOCTYPE HTML>
<html>
    <head>
        <title>Rue de France :: Calc</title>
        <link rel="stylesheet" href="style.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="jquery-1.7.min.js" type="text/javascript"></script>
        <script src="jquery.timers-1.2.js" type="text/javascript"></script>
        <script src="script.js" type="text/javascript"></script>
    </head>
    <body>
        <div id="container">    
            <div id="header">
                <div id="header-text">
                    <h1><a href="index.html">Rue de France</a></h1>
                    <h2><a href="contact.html">:: Calc</a></h2>
                </div>
                <div id="menu-bar">
                    <ul>
                        <li><a href="index.html">Home</a></li>
                        <li><a href="accommodations.html">Accommodatie</a></li>
                        <li><a href="environment.html">Omgeving</a></li>
                        <li><a href="pictures.html">Foto's</a></li>
                        <li class="menu-bar-current"><a href="contact.html">Contact</a></li>
                    </ul>
                </div>
                <a href="index.html"><div id="logo"></div></a>    
            </div>
            <div id="menu-left">
                <h1>Kortingen deze week</h1>
                <p>Deze week hebben wij velen kortingen in ons arrangement</p>
                <h1>Adres</h1>
                <p>
                    Frankrijkweg 19<br />
                    5975 PC, Sevenum
                </p>
            </div>
            <div id="content">
                <h1>Calc</h1>
		<small>Input 2 is not required in case of square root and power calculations</small>
		<?php
			$input1Value = (isset($_POST['input1'])) ? $_POST['input1'] : "";
			$input2Value = (isset($_POST['input2'])) ? $_POST['input2'] : "";
		?>
                <form method="post">
			<input type="text" name="input1" placeholder="Input 1" value="<?php echo $input1Value; ?>" />
			<input type="text" name="input2" placeholder="Input 2" value="<?php echo $input2Value; ?>" />
			<select name="operation">
				<option value="addition">Add</option>
				<option value="substraction">Substract</option>
				<option value="division">Divide</option>
				<option value="multiplication">Multiply</option>
				<option value="sqrt">Square root</option>
				<option value="power">Power</option>
			</select>
			<input type="submit" value="Calc" />
		</form>
		<?php
			$answer = 0;
			$operation = (isset($_POST['operation'])) ? $_POST['operation'] : "";
			switch ($operation) {
				case "addition":
					$answer = $input1Value + $input2Value;
					break;
				case "substraction":
					$answer = $input1Value - $input2Value;
					break;
				case "division":
					if ($input2Value == 0.0) {
						echo "The divisor can't be zero!";
						break;
					}
					$answer = $input1Value / $input2Value;
					break;
				case "multiplication":
					$answer = $input1Value * $input2Value;
					break;
				case "sqrt":
					$answer = sqrt($input1Value);
					break;
				case "power":
					$answer = pow($input1Value, (isset($_POST['input2'])) ? $_POST['input2'] : 2);
					break;
			}
			if ($answer) {
				echo "Answer: " + $answer;
			}
		?>
            </div>
            <!--<div id="menu-right"></div>-->
        </div>
        <div id="warning">
            <a href="#" onclick="warning.closeWarning()"><div id="warning_close_button"></div></a>
            Deze website is gebouwd om op een hogere resolutie te werken.<br />
            Mogelijk wordt de website niet goed weergegeven.<br /><br />
            <a href="#" onclick="warning.closeWarning()"><div id="warning_close_text">Sluit</div></a>
        </div>
    </body>
</html>
